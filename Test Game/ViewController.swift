//
//  ViewController.swift
//  Test Game
//
//  Created by Matthew Walker on 9/1/16.
//  Copyright © 2016 Element Media & Design. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    struct Hero {
        var characterName = ""
        var characterLevel = 1
        var charClass = ""
        var statPoints:Double = 12.0
        var str:Double = 8.0
        var con:Double = 8.0
        var dex:Double = 8.0
        var int:Double = 8.0
        var wis:Double = 8.0
        var cha:Double = 8.0
    }
    var h = Hero()
    
    @IBOutlet weak var characterField: UITextField!
    @IBOutlet weak var characterFinal: UILabel!
    @IBOutlet weak var charClass: UILabel!
    @IBOutlet weak var characterClass: UILabel!
    @IBOutlet weak var strStepperValue: UIStepper!
    @IBOutlet weak var conStepperValue: UIStepper!
    @IBOutlet weak var dexStepperValue: UIStepper!
    @IBOutlet weak var intStepperValue: UIStepper!
    @IBOutlet weak var wisStepperValue: UIStepper!
    @IBOutlet weak var chaStepperValue: UIStepper!
    @IBOutlet weak var strLabel: UILabel!
    @IBOutlet weak var conLabel: UILabel!
    @IBOutlet weak var dexLabel: UILabel!
    @IBOutlet weak var intLabel: UILabel!
    @IBOutlet weak var wisLabel: UILabel!
    @IBOutlet weak var chaLabel: UILabel!
    @IBOutlet weak var pointsLeft: UILabel!
    
    @IBAction func setCharacterName(sender: UIButton) {
        
        h.characterName = characterField.text!
        characterFinal.text = h.characterName
        
    }

    @IBAction func setClass(sender: UISegmentedControl) {
        
        var charClass:String?
        switch sender.selectedSegmentIndex {
            case 0:
                charClass = "Warrior"
            case 1:
                charClass = "Paladin"
            case 2:
                charClass = "Warlock"
            case 3:
                charClass = "Mage"
            default:
                break;
        }
        
        h.charClass = charClass!
        characterClass.text = charClass
        
    }
    
    @IBAction func strStepper(sender: UIStepper) {
        
        if Double(strLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(strLabel.text!)!
            if changeValue == 1.0 {
                strLabel.text = String(sender.value)
                h.str += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                strLabel.text = String(sender.value)
                h.str -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
    }
    
    @IBAction func conStepper(sender: UIStepper) {
        
        if Double(conLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(conLabel.text!)!
            if changeValue == 1.0 {
                conLabel.text = String(sender.value)
                h.con += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                conLabel.text = String(sender.value)
                h.con -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
    }
    
    @IBAction func dexStepper(sender: UIStepper) {
        
        if Double(dexLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(dexLabel.text!)!
            if changeValue == 1.0 {
                dexLabel.text = String(sender.value)
                h.dex += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                dexLabel.text = String(sender.value)
                h.dex -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
    }
    
    @IBAction func intStepper(sender: UIStepper) {
  
        if Double(intLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(intLabel.text!)!
            if changeValue == 1.0 {
                intLabel.text = String(sender.value)
                h.int += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                intLabel.text = String(sender.value)
                h.int -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
    }
    
    @IBAction func wisStepper(sender: UIStepper) {
        
        if Double(wisLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(wisLabel.text!)!
            if changeValue == 1.0 {
                wisLabel.text = String(sender.value)
                h.wis += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                wisLabel.text = String(sender.value)
                h.wis -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
    }
    
    @IBAction func chaStepper(sender: UIStepper) {
        
        if Double(chaLabel.text!)! >= 8.0 && Double(pointsLeft.text!)! > 0.0 {
            var changeValue: Double = sender.value - Double(chaLabel.text!)!
            if changeValue == 1.0 {
                chaLabel.text = String(sender.value)
                h.cha += 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! - 1)
            }
            else if changeValue == -1.0 {
                chaLabel.text = String(sender.value)
                h.cha -= 1.0
                pointsLeft.text = String(Double(pointsLeft.text!)! + 1)
            }
        }
        
    }
    
    @IBAction func changeView1(sender: UIButton) {
        self.performSegueWithIdentifier("moveStats", sender: self)
    }
    
    @IBAction func changeView2(sender: UIButton) {
        self.performSegueWithIdentifier("moveGame", sender: self)
    }
    
    
}

